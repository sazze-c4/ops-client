"use strict";

const _ = require('lodash');
const $ = require('sand-dollar');
const request = require('request');
const moment = require('moment');
const querystring = require('querystring');
const URL = require('url');
const Path = require('path');
const DEBUG = false;

class OpsClient {

  constructor(baseUrl, ttl, authToken) {
    this.baseUrl = baseUrl;
    this.ttl = ttl || 0;
    this.authToken = authToken;
  }

  login(user, password) {
    return this.send('post', '/login', {username: user, password: password});
  }

  host(fqdn, opts) {
    return this.send('get', `/host/${fqdn}`, opts);
  }

  find(query, opts) {
    return this.send('get', '/host/filter', _.defaults({q: query}, opts || {}));
  }

  checkVMStatus(fqdn, opts) {
    return this.send('get', `/host/${fqdn}/checkVMStatus`, opts);
  }

  provision(fqdn, opts) {
    return this.send('get', `/host/${fqdn}/provision`, opts);
  }

  deprovision(fqdn, opts) {
    return this.send('get', `/host/${fqdn}/deprovision`, opts);
  }

  reprovision(fqdn, opts) {
    return this.send('get', `/host/${fqdn}/reprovision`, opts);
  }

  setStatus(fqdn, status, opts) {
    return this.send('post', `/host/${fqdn}/setStatus`, _.merge({}, opts, {status: status ? 1 : 0}));
  }

  createBridge(fqdn, bridgeName, interfaceName, opts) {
    return this.send('post', `/host/${fqdn}/createBridge` , _.merge({}, opts, {bridgeName: bridgeName, interfaceName: interfaceName}));
  }

  deleteBridge(fqdn, bridgeName, opts) {
    return this.send('delete', `/host/${fqdn}/deleteBridge`, _.merge({}, opts, {bridgeName: bridgeName}));
  }

  createZone(name, domain, chefServer, opts) {
    return this.send('post', '/zone', _.merge({}, opts, {name: name, domain: domain, chefServer: chefServer}));
  }

  deleteZone(zoneId, opts) {
    return this.send('delete', `/zone/${zoneId}`, opts);
  }

  listZones(opts) {
    return this.send('get', `/zone/list`, opts);
  }

  showZone(zoneId, opts) {
    return this.send('get', `/zone/${zoneId}`, opts);
  }

  // library methods

  send(method, url, params, headers) {
    method = method.toUpperCase();
    params = params || {};
    headers = headers || {};

    if (this.authToken) {
      headers['x-auth-token'] = this.authToken;
    }

    let parts = URL.parse(this.baseUrl);
    let opts = {
      method: method,
      url: parts.protocol + '//' + parts.host + Path.join(parts.path, url),
      json: true,
      headers: headers
    };

    if (-1 === ['GET', 'DELETE', 'HEAD'].indexOf(method)) {
      opts.body = params;
    } else {
      opts.qs = params;
    }

    OpsClient.log(opts);

    return new Promise(function(resolve, reject) {
      request(opts, function(err, resp, body) {
        err = err || body.error;
        if (err) {
          reject(err);
        } else {
          resolve(new Response(resp, body));
        }
      });
    });
  }

  static log(opts) {
    if (DEBUG) {
      console.log(opts.method + ' ' + opts.url + '?' + querystring.stringify(opts.qs));
      if (opts.body) {
        let body = _.merge({}, opts.body);
        delete body.password;
        console.log(body);
      }
    }
  }

}

module.exports = OpsClient;

class Response {
  constructor(resp, body) {
    this.resp = resp;
    this.body = body;
  }

  get result() {
    return this.body ? this.body.result : {};
  }
}

OpsClient.Response = Response;
